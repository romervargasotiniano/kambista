import test from 'ava';
const request = require('supertest');
const app = require('./../server.js');

test('Validating register (OK) response', async t => {
  const email = 'kambista@kambista.com';
  const password = 'password';
  const response = await request(app)
    .post('/register')
    .send({email, password});

    t.is(response.status, 200);
    t.is(response.body.code, 201);
    t.is(response.body.msg, `SAVED`);
});

test('Validating register (Wrong email | Ok Password) error response', async t => {
  const email = 'asdal.com';
  const password = 'password';
  const response = await request(app)
    .post('/register')
    .send({email, password});

    t.is(response.status, 422);
    t.is(response.body.code, 998);
    t.is(response.body.msg, `INVALID`);
});

test('Validating register (Ok email | Diff Valid password) error response', async t => {
  const email = 'emai@email.com';
  const password = 'x';
  const response = await request(app)
    .post('/register')
    .send({email, password});

    t.is(response.status, 422);
    t.is(response.body.code, 998);
    t.is(response.body.msg, `INVALID`);
});

test('Validating register (Ok email | No password) error response', async t => {
  const email = 'emai@email.com';
  const response = await request(app)
    .post('/register')
    .send({email});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating register (Wrong email | Nod password) error response', async t => {
  const email = 'emai_email.com';
  const response = await request(app)
    .post('/register')
    .send({email});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating register (No email | Ok password) error response', async t => {
  const password = '1234567';
  const response = await request(app)
    .post('/register')
    .send({password});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating register (No email | Wrong password) error response', async t => {
  const password = '123';
  const response = await request(app)
    .post('/register')
    .send({password});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating register (No params) error response', async t => {
  const response = await request(app)
    .post('/register')
    .send();

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating register (Duplicate @ email, Ok password) error response', async t => {
  const email = 'k@mb1st@@em@1l.com';
  const password = 'lilpwd123';
  const response = await request(app)
    .post('/register')
    .send({email, password});

    t.is(response.body.code, 997);
    t.is(response.body.msg, `INVALID`);
});

/****************************************************************************************/

test('Validating login (OK) response', async t => {
  const email = 'correo@kambista.com';
  const password = 'password';
  const response = await request(app)
    .post('/login')
    .send({email, password});

    t.is(response.status, 200);
    t.is(response.body.code, 201);
    t.is(response.body.msg, `LOGIN`);
});

test('Validating login (Ok email | Wrong Password) error response', async t => {
  const email = 'correo@kambista.com';
  const password = 'pwd';
  const response = await request(app)
    .post('/login')
    .send({email, password});

    t.is(response.status, 401);
    t.is(response.body.code, 997);
    t.is(response.body.msg, `INVALID`);
});

test('Validating login (Ok email | Diff Valid Password) error response', async t => {
  const email = 'correo@kambista.com';
  const password = 'password1';
  const response = await request(app)
    .post('/login')
    .send({email, password});

    t.is(response.status, 401);
    t.is(response.body.code, 997);
    t.is(response.body.msg, `INVALID`);
});

test('Validating login (Ok email | No Password) error response', async t => {
  const email = 'correo@kambista.com';
  const response = await request(app)
    .post('/login')
    .send({email});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating login (Wrong email | No Password) error response', async t => {
  const email = 'correo_kambista.com';
  const response = await request(app)
    .post('/login')
    .send({email});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating login (Diff Valid email | No Password) error response', async t => {
  const email = 'correo@camvista.com';
  const response = await request(app)
    .post('/login')
    .send({email});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating login (Wrong email | Ok Password) error response', async t => {
  const email = 'correo_kambista.com';
  const password = 'password';
  const response = await request(app)
    .post('/login')
    .send({email, password});

    t.is(response.status, 401);
    t.is(response.body.code, 997);
    t.is(response.body.msg, `INVALID`);
});

test('Validating login (Diff valid email | Ok Password) error response', async t => {
  const email = 'emai@email.com';
  const password = 'password';
  const response = await request(app)
    .post('/login')
    .send({email, password});

    t.is(response.status, 401);
    t.is(response.body.code, 997);
    t.is(response.body.msg, `INVALID`);
});

test('Validating login (No email | Ok Password) error response', async t => {
  const password = 'password';
  const response = await request(app)
    .post('/login')
    .send({password});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating login (No email | Wrong Password) error response', async t => {
  const password = 'pd';
  const response = await request(app)
    .post('/login')
    .send({password});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating login (No email | Diff Valid Password) error response', async t => {
  const password = 'p@ssw0rd';
  const response = await request(app)
    .post('/login')
    .send({password});

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});

test('Validating login (No params) error response', async t => {
  const response = await request(app)
    .post('/login')
    .send();

    t.is(response.status, 422);
    t.is(response.body.code, 999);
    t.is(response.body.msg, `REQUIRED`);
});