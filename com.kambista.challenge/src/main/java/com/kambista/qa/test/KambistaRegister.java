package com.kambista.qa.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.kambista.qa.tool.Util;

public class KambistaRegister {
	private static final String WEB_DRIVER 			= "webdriver.chrome.driver";
	private static final String PATH_WEB_DRIVER 	= "C:\\Users\\varro7e\\Downloads\\chromedriver_win32\\chromedriver.exe";
	private static final String URL_REGISTER 		= "http://localhost:4000/register";
	private static final String SAVED 				= "SAVED";
	private static final String INVALID 			= "INVALID";
	private static final String REQUIRED 			= "REQUIRED";
	private static final String ID_EMAIL 			= "email";
	private static final String ID_PASS				= "password";
	private static final String ID_REGISTER 		= "register";
	private static final String ID_MSG 				= "msg";

	/** 
	 * Both WEB_DRIVER and PATH_WEB_DRIVER have to be same type
	 * In order to know which web driver version you should use, follow this link: https://www.seleniumhq.org/projects/webdriver/
	 * Note: take into account web browser version needs to be compatible with web driver (i.e: Google Chrome: http://chromedriver.chromium.org/getting-started)
	 * 
	 * */
	public KambistaRegister() {
		System.setProperty(WEB_DRIVER, PATH_WEB_DRIVER);
	}

	public boolean registerFunction(String txtEmail, String txtPassword, String txtExpectedMsg) throws InterruptedException {
        WebDriver driver =  new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(URL_REGISTER);

        if(txtEmail != null) driver.findElement(By.id(ID_EMAIL)).sendKeys(txtEmail);
        if(txtPassword != null) driver.findElement(By.id(ID_PASS)).sendKeys(txtPassword);

        Thread.sleep(1000);
        driver.findElement(By.id(ID_REGISTER)).click();
        Thread.sleep(1000);

        return txtExpectedMsg.equals(driver.findElement(By.id(ID_MSG)).getText());
	}

	public static void main(String[] args) throws InterruptedException {
		KambistaRegister kbr = new KambistaRegister();
		int ok = 0;
		int fail = 0;

		/**
		 * Test Scenarios
		 * If you want to read the names, please take a look at Postman's collection
		 */
		if (kbr.registerFunction("emai@email.com", "clave123", SAVED)) ok++; else fail++;
		if (kbr.registerFunction("asdal.com", "pxsswordd", INVALID)) ok++; else fail++;
		if (kbr.registerFunction("emai2@email.com", "x", INVALID)) ok++; else fail++;
		if (kbr.registerFunction("emai3@email.com", null, REQUIRED)) ok++; else fail++;
		if (kbr.registerFunction("k@mb1st@@em@1l.com", "lilpwd123", INVALID)) ok++; else fail++;
		if (kbr.registerFunction("emai_email.com", null, REQUIRED)) ok++; else fail++;
		if (kbr.registerFunction(null, "1234567", REQUIRED)) ok++; else fail++;
		if (kbr.registerFunction(null, "123", REQUIRED)) ok++; else fail++;
		if (kbr.registerFunction(null, null, REQUIRED)) ok++; else fail++;

		Util.printResult(ok, fail);
    }
}