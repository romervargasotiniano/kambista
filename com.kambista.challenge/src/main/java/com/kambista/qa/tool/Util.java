package com.kambista.qa.tool;

public class Util {
	private Util() {}

	public static void printResult(int ok, int fail) {
		System.out.println("---------------------\n|   OK   |   FAIL   |\n---------------------\n|   \" + ok + \"    |     \" + fail + \"    |\\n---------------------");
	}
}